from app import app
from flask import render_template,flash, redirect
from app.models.user_model import User
from app.forms.login_form import LoginForm
from flask import request
from flask_login import current_user, login_user,logout_user
from app import db
from flask.helpers import url_for


@app.route('/login',methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index_view'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid email or password')
            return redirect(url_for('login'))
        login_user(user)
        return redirect(url_for('index_view'))
    return render_template('login_temp.html',form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))