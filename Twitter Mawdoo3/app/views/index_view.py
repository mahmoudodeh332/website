from app import app
from flask import render_template,redirect,request
from flask_login import login_required, current_user
from app.forms.post_form import PostForm,CommentForm
from app import db
from app.models.post_model import Post
from app.models.comment_model import Comment
from flask.helpers import url_for
import requests


@app.route('/',methods=['GET', 'POST'])
@login_required
def index_view():
    post_form = PostForm()
    response = requests.get('http://127.0.0.1:5000/posts')
    try:
        tweets = response.json()['posts']
    except:
        tweets = {}
    print(tweets)
    if post_form.validate_on_submit():
        post = Post(body=post_form.post.data,author=current_user)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('index_view'))
    
    
    comments_res =requests.get('http://127.0.0.1:5000/get_comments')
    try:
        comments = comments_res.json()['comments']
    except:
        comments = {}

    return render_template('index.html',post_form=post_form, tweets = tweets, comments = comments)