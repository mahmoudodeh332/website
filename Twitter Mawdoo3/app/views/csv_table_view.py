from app import app
from flask import render_template,redirect,request
import pandas as pd 
import os
from flask.helpers import url_for
from app.config import Config


templates_dir = Config.TEMPLATES_PATH
@app.route('/csv_table')
def csv_table_view():
    df = pd.read_csv(os.path.join(os.path.dirname(__file__),'Divvy_Trips.csv'))

    df = df[1:300]
    df.to_html(f'{templates_dir}/divvy_table.htm')
    return render_template('csv_table.html')