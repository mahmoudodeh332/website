from app import app
from flask.helpers import url_for
from flask import render_template,flash, redirect
from flask_login import current_user, login_user
from app.forms.register_form import RegistrationForm
from app.models.user_model import User
from datetime import datetime
from app import db

@app.route('/register',methods=['GET', 'POST'])
def register_view():
    if current_user.is_authenticated:
        return redirect(url_for('index_view'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(name=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register_temp.html',form=form)