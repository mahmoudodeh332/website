from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField,TextAreaField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo,Length
from app.models.user_model import User


class LoginForm(FlaskForm):
    email = StringField('Enter Your Email', validators=[DataRequired(), Email()])
    password = PasswordField('Enter Your Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')