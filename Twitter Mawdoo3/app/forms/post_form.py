from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField,TextAreaField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo,Length




class PostForm(FlaskForm):
    post = TextAreaField('Share A Tweet', validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField('Tweet')




class CommentForm(FlaskForm):
    comment = TextAreaField('Comment', validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField('Comment')