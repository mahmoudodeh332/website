
from app import db
from datetime import datetime
from flask_login import UserMixin

class Post(db.Model,UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    likes = db.relationship('Like', backref='author', lazy='dynamic')
    comments = db.relationship('Comment', backref='author', lazy='dynamic')
    hashtags = db.relationship('Hashtag', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<Post {}>'.format(self.body)

    