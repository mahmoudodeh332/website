from flask import jsonify, request, abort,make_response
from app import db
from app import app
from app.models.post_model import Post
from app.models.like_model import Like
from app.models.comment_model import Comment
from app.models.user_model import User


import uuid


@app.route('/posts', methods=['GET'])
def get_all_posts():
    posts= Post.query.all()
    output = []
    if not posts:
        return jsonify({'message':'No posts!'}),404

    for post in posts:
        post_data = {}
        post_data['id'] =post.id
        post_data['body']= post.body
        post_data['username'] = post.author.name
        output.append(post_data)

    return jsonify({'posts':output})

@app.route('/post_likes/<post_id>', methods=['GET'])
def get_post_likes(post_id):
    likes = Like.query.filter_by(post_id=post_id).all()
    users = Like.query.filter_by(post_id=post_id).all()
    users_list = []
    for user in users:
        users_list.append(user.user_id)
    return jsonify({'Likes_count':len(likes),'users':users_list})
   
@app.route('/likes', methods=['GET'])
def get_all_likes():
    likes= Like.query.all()
    output = []
    if not likes:
        return jsonify({'message':'No likes!'}),404

    for like in likes:
        like_data = {}
        like_data['id'] =like.id
        like_data['user_id']= like.user_id
        like_data['post_id'] = like.post_id
        output.append(like_data)

    return jsonify({'likes':output})

@app.route('/add_like/<post_id>/<user_id>', methods=['POST'])
def add_like(post_id,user_id):
    new_like = Like(post_id=post_id, user_id=user_id)
    db.session.add(new_like)
    db.session.commit()
    return jsonify({'message':f'New like has been added from {user_id} to {post_id}'})


@app.route('/delete_like/<user_id>', methods=['DELETE'])
def delete_like(user_id):
    like = Like.query.filter_by(user_id=user_id).first()
    if not like:
        return jsonify({'message':'Like not Found!!!!'}),404
    db.session.delete(like)
    db.session.commit()
    return jsonify({'message': 'Like has been deleted!'}),200



@app.route('/get_comments', methods=['GET'])
def get_comments():
    comments= Comment.query.all()
    output = []
    if not comments:
        return jsonify({'comments':'No comments!'}),404

    for comment in comments:
        username = User.query.filter_by(id=comment.user_id).first()
        comment_data = {}
        comment_data['id'] =comment.id
        comment_data['user_id'] = comment.user_id
        comment_data['post_id'] =comment.post_id
        comment_data['body']= comment.body
        comment_data['username']= username.name
        output.append(comment_data)

    return jsonify({'comments':output})

@app.route('/add_comment/<post_id>/<user_id>/<body>', methods=['POST'])
def add_comments(post_id,user_id,body):
    new_comment = Comment(post_id=post_id,user_id=user_id,body=body)
    db.session.add(new_comment)
    db.session.commit()
    return jsonify({'message':f'New Comment has been added from {user_id} to {post_id}'})





