import numpy as np
import pandas as pd
import random
import numpy as np

df = pd.read_csv('Divvy_Trips_2015-Q1.csv')

starttime =pd.to_datetime( df['starttime'])
stoptime = pd.to_datetime(df['stoptime'])

c = 0
list_of_minutes = []
for i in starttime:
    start_year = i.to_pydatetime().year
    start_month = i.to_pydatetime().month
    start_day = i.to_pydatetime().day
    start_hour = i.to_pydatetime().hour
    start_minute = i.to_pydatetime().minute

    stop_year = stoptime[c].to_pydatetime().year
    stop_month = stoptime[c].to_pydatetime().month
    stop_day = stoptime[c].to_pydatetime().day
    stop_hour = stoptime[c].to_pydatetime().hour
    stop_minute = stoptime[c].to_pydatetime().minute

    years = stop_year - start_year
    months = stop_month - start_month
    days = stop_day - start_day
    hours = stop_hour - start_hour
    minutes = stop_minute - start_minute

    years_to_minutes = abs(years) * 525.600
    months_to_minutes = abs(months) * 43.8291
    days_to_minutes = abs(days) * 1.440
    hours_to_minutes = abs(hours) * 60
    duration_in_minutes = years_to_minutes + months_to_minutes + months_to_minutes + days_to_minutes + hours_to_minutes + abs(
        minutes)
    list_of_minutes.append(int(duration_in_minutes))
    c += 1
#df.insert(3, "time_in_minutes", list_of_minutes , True)
#df.drop(df.columns[3], axis=1,inplace=True)
# AVG
#df.insert(3, "time_in_minutes", list_of_minutes , True)
customers = df[df['usertype']=='Customer']
random_numbers = []
for i in range(len(customers)):
    random_numbers.append(random.randint(1940,1999))

m = df['birthyear'].isnull()
l = m.sum()
s = np.random.choice(random_numbers, size=l)
df.loc[m, 'birthyear'] = s
df.to_csv('Divvy_Trips_2015-Q1.csv')



avg_of_time_trip = df['time_in_minutes'].mean()
females = df[df['gender']=='Female']
percentage_of_females = (len(females) / len(df)) * 100
customers = df[df['usertype']=='Customer']
youngest = max(customers['birthyear'])
oldest = min(customers['birthyear'])

print(f'The average trip time in minutes: {avg_of_time_trip}\n The percentage of female customers: {percentage_of_females}\n '
      f'Youngest : {2015 - youngest}\n Oldest: {2015 - oldest}')