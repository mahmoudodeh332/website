
import re

def checkIntertwined(t1,t2):
    if int(f'{t1[0]}{t1[1]}') < int(f'{t2[2]}{t1[3]}') and int(f'{t1[2]}{t1[3]}') < int(f'{t2[2]}{t2[3]}'):
        return True
    return False

def sortTime(time_span1,time_span2):
    l =[]
    t1 = re.search('([0-9][0-9]).*([0-9][0-9]).*([0-9][0-9]).*([0-9][0-9])',time_span1).groups()
    t2 = re.search('([0-9][0-9]).*([0-9][0-9]).*([0-9][0-9]).*([0-9][0-9])',time_span2).groups()
    if checkIntertwined(t1,t2):
        l.append([f'{t1[0]}:{t1[1]}', f'{t2[2]}:{t2[3]}'])
        print(l)
    else:
        l.append([f'{t2[0]}:{t2[1]}-{t2[2]}:{t2[3]}'])
        l.append([f'{t1[0]}:{t1[1]}-{t1[2]}:{t1[3]}'])
        print(l)


sortTime(input('time_span1: '),input('time_span2: '))

# sortTime("[\"12:00\", \"14:00\"]","[\"13:00\", \"15:00\"]")
# sortTime("[\"17:00\", \"20:00\"]","[\"01:00\", \"02:40\"]")

# ● example 1:
# ○ input:
# ■ time_span1 = ["12:00", "14:00"]
# ■ time_span2 = ["13:00", "15:00"]
# ○ Output should be:
# ■ [["12:00", "15:00"]]


# ● example 2:
# ○ input:
# ■ time_span1 = ["17:00", "20:00"]
# ■ time_span2 = ["01:00", "02:40"]
# ○ Output should be:
# ■ [[01:00-02:40], [17:00-20:00]]